from src.directbank.models import Packet


class TestPacketModel:
    def test_one(self):
        x = "thish"
        assert "h" in x

    def test_two(self):
        x = "hello"
        assert not hasattr(x, "check")
